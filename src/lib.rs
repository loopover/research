#[macro_use]
extern crate smallvec;
extern crate rand;

pub mod board;
pub mod scramble;
pub mod search;

pub use self::board::*;

#[derive(Copy, Clone, PartialEq)]
pub enum Axis {
    Column,
    Row
}

#[derive(Copy, Clone, PartialEq)]
pub struct Move {
    pub axis: Axis,
    pub index: usize,
    pub n: isize
}

impl Move {
    pub fn new(axis: Axis, index: usize, n: isize) -> Move {
        Move { axis, index, n }
    }

    pub fn reverse(&self, board: &Board) -> Move {
        Move { axis: self.axis, index: self.index, n: match self.axis {
            Axis::Column => board.rows as isize - self.n,
            Axis::Row => board.cols as isize - self.n
        } }
    }
}

impl std::fmt::Debug for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}{}{}", self.n, match self.axis { Axis::Column => "C", Axis::Row => "R" }, self.index)
    }
}
