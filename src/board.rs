use smallvec::SmallVec;
use super::{Axis, Move};

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Board {
    pub rows: u8, pub cols: u8,
    pub grid: SmallVec<[SmallVec<[u8; 4]>; 4]>
}

impl Board {
    pub fn new(cols: u8, rows: u8) -> Board {
        Board {
            cols, rows,
            grid: (0..rows).map(|y| (0..cols).map(|x| y * cols + x).collect()).collect()
        }
    }

    pub fn is_solved(&self) -> bool {
        for (r, row) in self.grid.iter().enumerate() {
            for (c, tile) in row.iter().enumerate() {
                if *tile as usize != r * self.cols as usize + c { return false }
            }
        }
        true
    }

    pub fn is_solvable(&self) -> bool {
        if (self.cols * self.rows) % 2 == 0 { return true }

        let mut indices = Vec::new();
        for row in &self.grid { for tile in row { indices.push(tile + 1) } }

        let mut inversions = 0;
        for i in 0..indices.len() {
            for j in i + 1..indices.len() {
                if indices[i] > indices[j] {
                    inversions += 1;
                }
            }
        }
        inversions % 2 == 0
    }

    pub fn move_forward(&mut self, mov: Move) {
        match mov.axis {
            Axis::Row => self.move_row(mov.index, mov.n),
            Axis::Column => self.move_col(mov.index, mov.n),
        }
    }

    pub fn move_reverse(&mut self, mov: Move) {
        match mov.axis {
            Axis::Row => self.move_row(mov.index, self.cols as isize - mov.n),
            Axis::Column => self.move_col(mov.index, self.rows as isize - mov.n),
        }
    }

    pub fn move_row(&mut self, i: usize, n: isize) {
        let row = self.grid[i].clone();
        for (col, tile) in self.grid[i].iter_mut().enumerate() {
            *tile = row[((self.cols as isize + col as isize - n) % self.cols as isize) as usize];
        }
    }

    pub fn move_col(&mut self, i: usize, n: isize) {
        let col = (0..self.rows).map(|j| self.grid[j as usize][i]).collect::<Vec<_>>();
        for row in 0..self.rows {
            self.grid[row as usize][i] = col[(((self.rows + row) as isize - n) % self.rows as isize) as usize];
        }
    }

    pub fn pos(&self, i: u8) -> [isize; 2] {
        for r in 0..self.rows as usize {
            for c in 0..self.cols as usize {
                if self.grid[r][c] == i { return [c as isize, r as isize] }
            }
        }
        panic!()
    }
}

impl std::fmt::Debug for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for (r, row) in self.grid.iter().enumerate() {
            write!(f, "{}", if r > 0 { "\n\n" } else { "\n" })?;
            for i in row {
                write!(f, "{:5}", i + 1)?;
            }
        }
        write!(f, "\n")
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Board4 {
    pub grid: [[u8; 4]; 4]
}

impl Board4 {
    pub fn new() -> Board4 {
        Board4 {
            grid: [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15]]
        }
    }

    pub fn is_solved(&self) -> bool {
        for (r, row) in self.grid.iter().enumerate() {
            for (c, tile) in row.iter().enumerate() {
                if *tile as usize != r * 4 + c { return false }
            }
        }
        true
    }

    pub fn move_forward(&mut self, mov: Move) {
        match mov.axis {
            Axis::Row => self.move_row(mov.index, mov.n as usize),
            Axis::Column => self.move_col(mov.index, mov.n as usize),
        }
    }

    pub fn move_reverse(&mut self, mov: Move) {
        match mov.axis {
            Axis::Row => self.move_row(mov.index, 4 - mov.n as usize),
            Axis::Column => self.move_col(mov.index, 4 - mov.n as usize),
        }
    }

    pub fn move_row(&mut self, i: usize, n: usize) {
        let row = self.grid[i].clone();
        for (col, tile) in self.grid[i].iter_mut().enumerate() {
            *tile = row[(4 + col - n) % 4 as usize];
        }
    }

    pub fn move_col(&mut self, i: usize, n: usize) {
        let col = (0..4).map(|j| self.grid[j as usize][i]).collect::<Vec<_>>();
        for row in 0..4 {
            self.grid[row as usize][i] = col[((4 + row) as usize - n) % 4];
        }
    }
}
