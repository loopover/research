use super::{Board, Axis, Move};
use rand::prelude::*;

pub fn first_solution(board: &Board, max_depth: usize) -> Option<Vec<Move>> {
    deep_search(board, 0, Vec::new(), true, max_depth)
}

pub fn optimal_solution(board: &Board, max_depth: usize) -> Option<Vec<Move>> {
    deep_search(board, 0, Vec::new(), false, max_depth)
}

fn deep_search(board: &Board, depth: usize, moves: Vec<Move>, return_first: bool, max_depth: usize) -> Option<Vec<Move>> {
    if depth == max_depth { return None }
    let mut rng = thread_rng();
    let mut solution: Option<Vec<Move>> = None;

    for &axis in &{ let mut axes = [Axis::Column, Axis::Row]; axes.shuffle(&mut rng); axes } {
        'i: for index in { let mut indices = (0..if axis == Axis::Row { board.rows } else { board.cols }).collect::<Vec<_>>(); indices.shuffle(&mut rng); indices } {
            for n in { let mut ns = (1..if axis == Axis::Row { board.cols } else { board.rows }).collect::<Vec<_>>(); ns.shuffle(&mut rng); ns } {
                let mov = Move::new(axis, index as usize, n as isize);
                if moves.len() > 0 {
                    for &last_move in moves.iter().rev() {
                        if mov.axis == last_move.axis && mov.index == last_move.index {
                            continue 'i
                        }
                        if mov.axis != last_move.axis { break }
                    }
                }
                let mut board = board.clone();
                board.move_forward(mov);
                let mut moves = moves.clone();
                moves.push(mov);

                if board.is_solved() {
                    if return_first {
                        return Some(moves)
                    } else if let Some(prev_solution) = &solution {
                        if moves.len() < prev_solution.len() {
                            solution = Some(moves)
                        }
                    } else {
                        solution = Some(moves)
                    }
                } else {
                    if let Some(new_solution) = deep_search(&board, depth + 1, moves, return_first, max_depth) {
                        if return_first {
                            return Some(new_solution)
                        } else if let Some(prev_solution) = &solution {
                            if new_solution.len() < prev_solution.len() {
                                solution = Some(new_solution)
                            }
                        } else {
                            solution = Some(new_solution)
                        }
                    }
                }
            }
        }
    }

    solution
}