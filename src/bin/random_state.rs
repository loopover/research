extern crate loopover_research;

use loopover_research::*;

/*
https://www.cs.bham.ac.uk/~mdr/teaching/modules04/java2/TilesSolvability.html
https://www.geeksforgeeks.org/counting-inversions/
*/

fn main() {
    for _ in 0..50 {
        let mut board = Board::new(3, 3);
        scramble::random_state(&mut board);

        if is_solvable(&board) {
            if search::first_solution(&board, 8).is_some() {
                println!("Is solvable");
            } else {
                panic!();
            }
        } else {
            println!("Not solvable");
        }
    }
}

fn is_solvable(board: &Board) -> bool {
    if (board.cols * board.rows) % 2 == 0 { return true }

    let mut indices = Vec::new();
    for row in &board.grid { for tile in row { indices.push(tile + 1) } }

    let mut inversions = 0;
    for i in 0..indices.len() {
        for j in i + 1..indices.len() {
            if indices[i] > indices[j] {
                inversions += 1;
            }
        }
    }
    inversions % 2 == 0
}