extern crate loopover_research;

use loopover_research::*;
use std::collections::HashSet;
use std::time::*;

fn main() {
    let board = Board4::new();

    let mut sets: Vec<HashSet<Board4>> = Vec::new();
    let mut depth = 1;

    let start_time = Instant::now();
    loop {
        let mut set = HashSet::new();
        if depth == 1 {
            set.extend(get_next_possible_states(&board));
        } else {
            let last_set = sets.last().unwrap();

            for (i, board) in last_set.iter().enumerate() {
                if i % 250000 == 250000 - 1 { println!("{} %", (i * 1000 / last_set.len()) as f32 / 10.0) }

                let mut next_boards = Vec::new();
                for new_board in &get_next_possible_states(&board) {
                    if new_board == board { continue }

                    let mut exists_in_prev_depth = false;
                    for set in &sets { if set.contains(new_board) {
                        exists_in_prev_depth = true;
                        break
                    } }
                    if !exists_in_prev_depth {
                        next_boards.push(new_board.clone());
                    }
                }
                set.extend(next_boards);
            }

        }
        println!("depth {}, {} perms", depth, set.len());
        if set.len() == 0 { depth -= 1; break }
        sets.push(set);
        depth += 1;
    }

    let elapsed = start_time.elapsed();

    println!("The board has a depth of {}", depth);
    println!("Took {} seconds", elapsed.as_secs());
}

/*
(multi-move metric)
depth 1, 6 perms
depth 2, 19 perms
depth 3, 54 perms
depth 4, 162 perms
depth 5, 486 perms
depth 6, 1281 perms
depth 7, 1780 perms
depth 8, 988 perms
depth 9, 233 perms
depth 10, 29 perms
depth 11, 2 perms
depth 12, 0 perms
*/

fn get_next_possible_states(board: &Board4) -> Vec<Board4> {
    let mut boards = Vec::new();
    for index in 3..4 {
        for n in 1..4 {
            let mut board = board.clone();
            board.move_col(index, n);
            boards.push(board);
        }
    }
    for index in 3..4 {
        for n in 1..4 {
            let mut board = board.clone();
            board.move_row(index, n);
            boards.push(board);
        }
    }
    boards
}