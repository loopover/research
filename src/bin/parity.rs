extern crate loopover_research;

use loopover_research::*;

fn main() {
    let size = 4;
    let mut board = Board::new(size as u8, size as u8);
    let tmp = board.grid[size - 1][size - 1];
    board.grid[size - 1][size - 1] = board.grid[size - 1][size - 2];
    board.grid[size - 1][size - 2] = tmp;

    println!("{:?}", board);

    if let Some(solution) = deep_search(&board, 0, vec![], false, 8) {
        println!("{:?}", solution);
    }
}


fn deep_search(board: &Board, depth: usize, moves: Vec<Move>, return_first: bool, max_depth: usize) -> Option<Vec<Move>> {
    if depth == max_depth { return None }
    let mut solution: Option<Vec<Move>> = None;

    'a: for &axis in &[Axis::Column, Axis::Row] {
        for n in 1..if axis == Axis::Row { board.cols } else { board.rows } {
            let mov = Move::new(axis, board.cols as usize - 1, n as isize);
            if let Some(last_move) = moves.last() {
                if mov.axis == last_move.axis { continue 'a }
            }
            let mut board = board.clone();
            board.move_forward(mov);
            let mut moves = moves.clone();
            moves.push(mov);

            if board.is_solved() {
                if return_first {
                    return Some(moves)
                } else if let Some(prev_solution) = &solution {
                    if moves.len() < prev_solution.len() {
                        solution = Some(moves)
                    }
                } else {
                    solution = Some(moves)
                }
            } else {
                if let Some(new_solution) = deep_search(&board, depth + 1, moves, return_first, max_depth) {
                    if return_first {
                        return Some(new_solution)
                    } else if let Some(prev_solution) = &solution {
                        if new_solution.len() < prev_solution.len() {
                            solution = Some(new_solution)
                        }
                    } else {
                        solution = Some(new_solution)
                    }
                }
            }
        }
    }

    solution
}