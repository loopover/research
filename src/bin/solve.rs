extern crate loopover_research;

use loopover_research::*;
use self::scramble::scramble_board;
use std::time::Instant;

fn main() {
    let mut board = Board::new(3, 3);
    scramble_board(&mut board);

    println!("{:?}", board);
    let start = Instant::now();

    if let Some(solution) = search::optimal_solution(&board, 8) {
        println!("{:?}", solution);
    }
    println!("Took {} seconds", start.elapsed().as_secs_f32());
}
