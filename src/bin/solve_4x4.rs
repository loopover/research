extern crate loopover_research;

use loopover_research::*;
use rand::prelude::*;

fn main() {
    let mut board = Board::new(4, 4);
    scramble::scramble_board(&mut board);

    println!("{:?}", board);

    if let Some(solution) = solve_4x4(&board) {
        println!("Length: {:?}", solution.len());
        println!("{}", solution.iter().map(|a| format!("{:?}", a)).collect::<Vec<_>>().join(" "));
    }
}

fn solve_4x4(board: &Board) -> Option<Vec<Move>> {
    let mut board = board.clone();

    let mut block_2x2_solution = deep_search(&board, 0, vec![], false, 5)?;
    for &mov in &block_2x2_solution { board.move_forward(mov) }

    let mut block_3x3_solution = deep_search_2(&board, 0, vec![], false, 6)?;
    for &mov in &block_3x3_solution { board.move_forward(mov) }

    let mut lsll_solution = deep_search_3(&board, 0, vec![], false, 8)?;
    for &mov in &lsll_solution { board.move_forward(mov) }

    let mut solution = Vec::new();
    solution.append(&mut block_2x2_solution);
    solution.append(&mut block_3x3_solution);
    solution.append(&mut lsll_solution);
    Some(solution)
}

fn deep_search(board: &Board, depth: usize, moves: Vec<Move>, return_first: bool, max_depth: usize) -> Option<Vec<Move>> {
    if depth == max_depth { return None }
    let mut rng = thread_rng();
    let mut solution: Option<Vec<Move>> = None;

    for &axis in &{ let mut axes = [Axis::Column, Axis::Row]; axes.shuffle(&mut rng); axes } {
        'i: for index in { let mut indices = (0..if axis == Axis::Row { board.rows } else { board.cols }).collect::<Vec<_>>(); indices.shuffle(&mut rng); indices } {
            for n in { let mut ns = (1..if axis == Axis::Row { board.cols } else { board.rows }).collect::<Vec<_>>(); ns.shuffle(&mut rng); ns } {
                let mov = Move::new(axis, index as usize, n as isize);
                if moves.len() > 0 {
                    for &last_move in moves.iter().rev() {
                        if mov.axis == last_move.axis && mov.index == last_move.index {
                            continue 'i
                        }
                        if mov.axis != last_move.axis { break }
                    }
                }
                let mut board = board.clone();
                board.move_forward(mov);
                let mut moves = moves.clone();
                moves.push(mov);

                if board.grid[0][0] == 0 && board.grid[0][1] == 1 && board.grid[1][0] == 4 && board.grid[1][1] == 5 {
                    if return_first {
                        return Some(moves)
                    } else if let Some(prev_solution) = &solution {
                        if moves.len() < prev_solution.len() {
                            solution = Some(moves)
                        }
                    } else {
                        solution = Some(moves)
                    }
                } else {
                    if let Some(new_solution) = deep_search(&board, depth + 1, moves, return_first, max_depth) {
                        if return_first {
                            return Some(new_solution)
                        } else if let Some(prev_solution) = &solution {
                            if new_solution.len() < prev_solution.len() {
                                solution = Some(new_solution)
                            }
                        } else {
                            solution = Some(new_solution)
                        }
                    }
                }
            }
        }
    }

    solution
}


fn deep_search_2(board: &Board, depth: usize, moves: Vec<Move>, return_first: bool, max_depth: usize) -> Option<Vec<Move>> {
    if depth == max_depth { return None }
    let mut rng = thread_rng();
    let mut solution: Option<Vec<Move>> = None;

    for &axis in &{ let mut axes = [Axis::Column, Axis::Row]; axes.shuffle(&mut rng); axes } {
        'i: for index in { let mut indices = (2..if axis == Axis::Row { board.rows } else { board.cols }).collect::<Vec<_>>(); indices.shuffle(&mut rng); indices } {
            for n in { let mut ns = (1..if axis == Axis::Row { board.cols } else { board.rows }).collect::<Vec<_>>(); ns.shuffle(&mut rng); ns } {
                let mov = Move::new(axis, index as usize, n as isize);
                if moves.len() > 0 {
                    for &last_move in moves.iter().rev() {
                        if mov.axis == last_move.axis && mov.index == last_move.index {
                            continue 'i
                        }
                        if mov.axis != last_move.axis { break }
                    }
                }
                let mut board = board.clone();
                board.move_forward(mov);
                let mut moves = moves.clone();
                moves.push(mov);

                if board.grid[0][2] == 2 && board.grid[1][2] == 6 && board.grid[2][0] == 8 && board.grid[2][1] == 9 && board.grid[2][2] == 10 {
                    if return_first {
                        return Some(moves)
                    } else if let Some(prev_solution) = &solution {
                        if moves.len() < prev_solution.len() {
                            solution = Some(moves)
                        }
                    } else {
                        solution = Some(moves)
                    }
                } else {
                    if let Some(new_solution) = deep_search_2(&board, depth + 1, moves, return_first, max_depth) {
                        if return_first {
                            return Some(new_solution)
                        } else if let Some(prev_solution) = &solution {
                            if new_solution.len() < prev_solution.len() {
                                solution = Some(new_solution)
                            }
                        } else {
                            solution = Some(new_solution)
                        }
                    }
                }
            }
        }
    }

    solution
}


fn deep_search_3(board: &Board, depth: usize, moves: Vec<Move>, return_first: bool, max_depth: usize) -> Option<Vec<Move>> {
    if depth == max_depth { return None }
    let mut solution: Option<Vec<Move>> = None;

    'a: for &axis in &[Axis::Column, Axis::Row] {
        for n in 1..if axis == Axis::Row { board.cols } else { board.rows } {
            let mov = Move::new(axis, board.cols as usize - 1, n as isize);
            if let Some(last_move) = moves.last() {
                if mov.axis == last_move.axis { continue 'a }
            }
            let mut board = board.clone();
            board.move_forward(mov);
            let mut moves = moves.clone();
            moves.push(mov);

            if board.is_solved() {
                if return_first {
                    return Some(moves)
                } else if let Some(prev_solution) = &solution {
                    if moves.len() < prev_solution.len() {
                        solution = Some(moves)
                    }
                } else {
                    solution = Some(moves)
                }
            } else {
                if let Some(new_solution) = deep_search_3(&board, depth + 1, moves, return_first, max_depth) {
                    if return_first {
                        return Some(new_solution)
                    } else if let Some(prev_solution) = &solution {
                        if new_solution.len() < prev_solution.len() {
                            solution = Some(new_solution)
                        }
                    } else {
                        solution = Some(new_solution)
                    }
                }
            }
        }
    }

    solution
}