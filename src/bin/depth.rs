extern crate loopover_research;

use loopover_research::*;
use std::collections::HashSet;
use std::time::*;

fn main() {
    let cols = 3; let rows = 3;
    let board = Board::new(cols, rows);

    let mut sets: Vec<HashSet<Board>> = Vec::new();
    let mut depth = 1;

    let start_time = Instant::now();
    loop {
        let mut set = HashSet::new();
        if depth == 1 {
            set.extend(get_next_possible_states(&board));
        } else {
            let last_set = sets.last().unwrap();

            for (i, board) in last_set.iter().enumerate() {
                if i % 250000 == 250000 - 1 { println!("{} %", (i * 1000 / last_set.len()) as f32 / 10.0) }

                let mut next_boards = Vec::new();
                for new_board in &get_next_possible_states(&board) {
                    if new_board == board { continue }

                    let mut exists_in_prev_depth = false;
                    for set in &sets { if set.contains(new_board) {
                        exists_in_prev_depth = true;
                        break
                    } }
                    if !exists_in_prev_depth {
                        next_boards.push(new_board.clone());
                    }
                }
                set.extend(next_boards);
            }

        }
        println!("depth {}, {} perms", depth, set.len());
        if set.len() == 0 { depth -= 1; break }

        sets.push(set);
        depth += 1;
    }

    let elapsed = start_time.elapsed();

    println!("The board has a depth of {}", depth);
    println!("Took {:.3} seconds", elapsed.as_secs_f32());
}

fn get_next_possible_states(board: &Board) -> Vec<Board> {
    let mut boards = Vec::with_capacity((board.cols * (board.rows - 1) + board.rows * (board.cols - 1)) as usize);
    for index in 0..board.cols as usize {
        for n in 1..board.rows as isize {
            let mut board = board.clone();
            board.move_col(index, n);
            boards.push(board);
        }
    }
    for index in 0..board.rows as usize {
        for n in 1..board.cols as isize {
            let mut board = board.clone();
            board.move_row(index, n);
            boards.push(board);
        }
    }
    boards
}