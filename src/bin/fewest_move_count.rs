extern crate loopover_research;

use loopover_research::*;
use self::scramble::scramble_board;
use std::collections::HashMap;

fn main() {
    let counts = count_optimal_solutions(&mut |mut board| {
        scramble_board(&mut board);
        board
    }, Board::new(3, 3), 100, 5);

    for key in { let mut x = counts.keys().collect::<Vec<_>>(); x.sort(); x} {
        if *key == 0 {
            println!("Not solved: {}", counts[key]);
        } else {
            println!("{} moves: {}", key, counts[key]);
        }
    }
}

/*
board = (3, 3), max_depth = 5, total_count = 1000
- smart scramble -> {0: 612, 2: 10, 3: 24, 4: 97, 5: 257}
- random scramble -> {0: 360, 1: 6, 2: 26, 3: 94, 4: 203, 5: 311}
*/

fn count_optimal_solutions(scramble_fn: &mut dyn FnMut(Board) -> Board,
    board: Board, total_count: usize, max_depth: usize) -> HashMap<usize, usize> {
    let mut map = HashMap::new();

    for _ in 0..total_count {
        let board = scramble_fn(board.clone());
        if let Some(solution) = search::optimal_solution(&board, max_depth) {
            if let Some(count) = map.get_mut(&solution.len()) {
                *count += 1;
            } else {
                map.insert(solution.len(), 1);
            }
        } else {
            if let Some(c) = map.get_mut(&0) { *c += 1; } else { map.insert(0, 1); }
        }
    }

    map
}
