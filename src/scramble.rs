use super::{scramble, Board, Move, Axis};
use rand::prelude::*;

pub fn scramble_board(board: &mut Board) {
    for _ in 0..20 {
        scramble::random_state(board);
        if board.is_solvable() { return }
    }
    panic!("Couldn't scramble board");
}

/**
 * Generate random state for board.
 * Can create unsolveable permutations for uneven board sizes.
 */
pub fn random_state(board: &mut Board) {
    let mut indices = (0..board.cols * board.rows).collect::<Vec<_>>();
    indices.shuffle(&mut thread_rng());
    for row in 0..board.rows as usize {
        for col in 0..board.cols as usize {
            board.grid[row][col] = indices[row * board.cols as usize + col];
        }
    }
}

/**
 * Avoids moves that undo actions of previous moves and same axis moves
 * for more that 2 times in a row.
 */
pub fn smart_moves(board: &Board, length: usize) -> Vec<Move> {
    let mut moves: Vec<Move> = Vec::new();
    for _ in 0..length {
        'outer: loop {
            let axis = *[Axis::Column, Axis::Row].choose(&mut thread_rng()).unwrap();
            let mov = Move::new(
                axis,
                random::<usize>() % if axis == Axis::Row { board.rows } else { board.cols } as usize,
                random::<isize>() % (if axis == Axis::Row { board.cols } else { board.rows } as isize - 1) + 1
            );
            let mut same_axis_in_a_row = 1;
            for last in moves.iter().rev() {
                if last.index == mov.index || same_axis_in_a_row == 2 { continue 'outer }
                if last.axis != mov.axis { break }
                same_axis_in_a_row += 1;
            }
            moves.push(mov); break
        }
    }
    moves
}

/**
 * Generates completely random axes, row / column indices and move amounts.
 */
pub fn random_moves(board: &Board, length: usize) -> Vec<Move> {
    let mut moves: Vec<Move> = Vec::new();
    for _ in 0..length {
        let axis = *[Axis::Column, Axis::Row].choose(&mut thread_rng()).unwrap();
        let mov = Move::new(
            axis,
            random::<usize>() % if axis == Axis::Row { board.rows } else { board.cols } as usize,
            random::<isize>() % (if axis == Axis::Row { board.cols } else { board.rows } as isize - 1) + 1
        );
        moves.push(mov);
    }
    moves
}