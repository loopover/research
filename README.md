# Loopover Research

This repository contains algorithms to solve and find the depth
of a certain board.

The [Rust Language](https://www.rust-lang.org) is used for the experiments.

## Depth

The depth of a puzzle is the number of moves it takes to get to any permutation.

`src/bin/depth.rs`

```
cargo run --release --bin depth
```

## Parity

> The parity of a permutation refers to whether that permutation is even or odd.
> An even permutation is one that can be represented by an even number of swaps
> while an odd permutation is one that can be represented by an odd number of swaps.
https://www.ryanheise.com/cube/parity.html

Parity can be created from a solved permutation on every even-numbered board and
therefore it is always solvable. For boards with and without different
aspect ratios than 1, the following applies:

When `cols * rows % 2 == 0` is true, it's an even-numbered board and parity can
happen while solving.

Since you can always switch two tiles in even-numbered boards,
it means that every board, that is initialized with randomly ordered indices will be solvable.

In order to find out if an oddly numbered board is solvable, the number of
inversions in the indices are counted and when they are even the board is solvable.
